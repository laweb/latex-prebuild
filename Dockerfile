FROM fedora:38
LABEL maintainer="Bengt Giger <bgiger@ethz.ch>"

RUN dnf update -y && dnf install -y texlive-tikz-dependency texlive-tex4ht texlive-scheme-full texlive-ifluatex


